<?php
/**
 * This class is used to read and write defined variables
 * inside of the wp-config.php file.
 * 
 * @author Gediminas Bernatonis
 * @copyright 2013
 */
class wpConfigController{
    private $wpConfigTokens = array();
    private $wpConfigFileDir='';
    public function __construct($wpConfigFileDir){
        if(!file_exists($wpConfigFileDir)){
            die('wp-config.php not found :/');
        }
        $this->wpConfigFileDir=$wpConfigFileDir;
        $wpConfigFile=file_get_contents($wpConfigFileDir);
        $wpConfigTokens = token_get_all($wpConfigFile);
        $this->wpConfigTokens=$this->token_rename_all($wpConfigTokens);
    }
    //save
    public function save_changes(){
        $file='';
        $tokenLen=count($this->wpConfigTokens);
        for($i=0;$i<$tokenLen;$i++){
            if(isset($this->wpConfigTokens[$i][1])){
                $file.=$this->wpConfigTokens[$i][1];
                //echo $this->wpConfigTokens[$i][1];
            }else{
                if(isset($this->wpConfigTokens[$i][0])){
                    $file.=$this->wpConfigTokens[$i][0];
                }else{
                    $file.=$this->wpConfigTokens[$i];
                }
            }
        }
        $save=file_put_contents($this->wpConfigFileDir, $file, LOCK_EX);
		return $save;
        //var_dump($save);
    }
    //databse name
    public function get_DBname(){
        $name=$this->get_defined_variable("DB_NAME");
        return $name['value'];
    }
    public function set_DBname($dbname){
        $this->set_defined_variable('DB_NAME', $dbname);
    }
    //database username
    public function get_DBuser(){
        $user=$this->get_defined_variable("DB_USER");
        return $user['value'];
    }
    public function set_DBuser($dbuser){
        $this->set_defined_variable('DB_USER', $dbuser);
    }
    //database password
    public function get_DBpassword(){
        $pass=$this->get_defined_variable("DB_PASSWORD");
        return $pass['value'];
    }
    public function set_DBpassword($dbpass){
        $this->set_defined_variable('DB_PASSWORD', $dbpass);
    }
    //database host
    public function get_DBhost(){
        $host=$this->get_defined_variable("DB_HOST");
        return $host['value'];
    }
    public function set_DBhost($dbhost){
        $this->set_defined_variable('DB_HOST', $dbhost);
    }
    //table prefix
    public function get_tablePrefix(){
        $tp=$this->get_variable('table_prefix');
        return $tp['value'];
    }
    public function set_tablePrefix($tp){
        $this->set_variable('table_prefix', $tp);
    }
    //setter
    private function set_variable($var, $value){
        $r=$this->get_variable($var);
        $this->wpConfigTokens[$r['tn']][1]="'$value'";
    }
    private function set_defined_variable($var, $value){
        $r=$this->get_defined_variable($var);
        $this->wpConfigTokens[$r['tn']][1]="'$value'";
    }
    //getter
    private function get_defined_variable($var){
        $var="'".$var."'";
        $constant_string_found=false;
        $db_name_string_found=false;
        $coma_found=false;
        $i=0;
        foreach($this->wpConfigTokens as $token){
            if(isset($token[0])){
                switch(trim($token[0])){
                    case 'T_CONSTANT_ENCAPSED_STRING':
                        $constant_string_found=true;
                        if($token[1]==$var){
                            $db_name_string_found=true;
                            break;
                        }
                        if($db_name_string_found==true && $coma_found==true){
                            $return=str_replace("'", '', $token[1]);
                            return array(
                                'value'=>$return,
                                'tn'=>$i
                            );
                        }
                    break;
                    case ',':
                        if($db_name_string_found==true){
                            $coma_found=true;
                        }
                    break;
                    case 'T_WHITESPACE':
                        //skip the default action
                    break;
                    default:
                        $constant_string_found=false;
                        $db_name_string_found=false;
                        $coma_found=false;
                    break;
                }
            }
            $i++;
        }
    }
    private function get_variable($var){
        $var='$'.$var;
        $t_variable_found=false;
        $var_found=false;
        $i=0;
        foreach($this->wpConfigTokens as $token){
            if(isset($token[0])){
                switch(trim($token[0])){
                    case 'T_VARIABLE':
                        $t_variable_found=true;
                        if($token[1]==$var){
                            $var_found=true;
                        }
                    break;
                    case 'T_CONSTANT_ENCAPSED_STRING':
                        if($t_variable_found==true && $var_found==true){
                            $return=str_replace("'", '', $token[1]);
                            return array(
                                'value'=>$return,
                                'tn'=>$i
                            );
                        }
                    break;
                    case '=':
                    case 'T_WHITESPACE':
                        //skip the default action
                    break; 
                    default:
                        $t_variable_found=false;
                        $var_found=false;
                    break;
                }
            }
            $i++;
        }
    }
    //this just helps use the tokens, bad for performance
    private function token_rename_all($tokens_array=array()){
        $return = array();
        foreach($tokens_array as $token){
            if(is_int($token[0])){
                $renamed=array(
                    0=>token_name($token[0]),
                    1=>$token[1],
                    2=>$token[2]
                );
            }else{
                $renamed=array(
                    0=>$token[0]
                );
            }
            array_push($return, $renamed);
        }
        //echo '<pre>'; print_r($return); echo '</pre>';
        return $return;
    }
}
?>