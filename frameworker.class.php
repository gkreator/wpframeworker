<?php
class Frameworker{
    private $database=null;
	public function __construct($framework, $name){
        $this->database=new Database('localhost', 'root', '', 'test');
        //var_dump($this->database);
        //die();
        //
		$preview_new = $this->preview_name_generate($name);
		$dir='C:\wamp\www\frameworker';
        $framework_dir=$dir.'\frameworks\\'.$framework;
        //check if already made :: prev
        $preview_dir=$dir.'\preview\\'.$this->preview_name_generate($name);
        if(is_dir($framework_dir)){
            if(!is_dir($preview_dir)){
                $this->recurse_copy($framework_dir, $preview_dir);
            }
            $wpCc=new wpConfigController($preview_dir.'\wp-config.php');
            //echo 'old dbname: '.$wpCc->get_DBname().'<br />';
            $dbname='template_'.$framework;
            $dbuser='template_'.$framework;
            $dbpass='asd123';
            $tp=$this->preview_name_generate($name).'_';
            $tp=str_replace('.', '', $tp);
            //update wp-config.php
            $wpCc->set_DBname($dbname);
            $wpCc->set_DBuser($dbuser);
            $wpCc->set_DBpassword($dbpass);
            $wpCc->set_DBhost('localhost');
            $wpCc->set_tablePrefix($tp);
            $wpCc->save_changes();
            //create db if needed
            if(!$this->db_exists($dbname)){
                $this->create_db($dbname, $dbuser, $dbpass);    
            }
            //install wp
            $install=new wpInstaller($preview_dir);
        }else{
            die('no framework folder OR');
        }
	}
    private function db_exists($dbname){
        //need api for cpanel prolly
        $result=$this->database->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$dbname'");
        //var_dump($result);
        if($result->num_rows){
            return true;
        }else{
            return false;
        }
    }
    private function create_db($dbname, $dbuser, $dbpass){
        //need api for cpanel
        $this->database->query("CREATE USER '$dbuser'@'localhost' IDENTIFIED BY '$dbpass';");
        $this->database->query("GRANT ALL ON $dbname.* TO '$dbuser'@'localhost'");
        $this->database->query("CREATE DATABASE $dbname");
    }
	private function preview_name_generate($name){
        $stripped = preg_replace("/[^A-Za-z0-9 ]/", '', $name);
        $parts=explode(' ', $stripped);
        $partsLen=count($parts);
        $new='';
        for($i=0;$i<$partsLen;$i++){
            if($i===0){
                $new.=strtolower($parts[$i]);    
            }else{
                $new.='.'.ucfirst($parts[$i]);
            }
        }
		return $new;
	}
	private function recurse_copy($src,$dst) { 
		$dir = opendir($src); 
		@mkdir($dst); 
		while(false !== ( $file = readdir($dir)) ) { 
			if (( $file != '.' ) && ( $file != '..' )) { 
				if ( is_dir($src . '/' . $file) ) { 
					$this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
				} 
				else { 
					copy($src . '/' . $file,$dst . '/' . $file); 
				} 
			} 
		} 
		closedir($dir); 
	} 
}
?>