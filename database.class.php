<?php
/* This file and all other VividCastle files are licensed under:
 * The GNU Lesser General Public License, version 3.0 (LGPL-3.0)
 * -------------------------------------------------------------
 * A copy of the license can be found in LICENSE.txt
 * Alternativily the license can be viewed at:
 * http://opensource.org/licenses/LGPL-3.0
 * -------------------------------------------------------------
 * Contact: Andrew Carlson <Andrew@HypnoRabbit.com>
 * Homepage: http://VividCastle.com
 * DO NOT REMOVE THIS NOTICE!
 */

/**
 * Database class
 * 
 * This class extends the MySQLi Class and adds performance improvments 
 * and extensive control over MySQL.
 * 
 * @author Andrew Carlson  <andrew@hypnorabbit.com>
 * @package Classes
 * @subpackage General
 * @version 1.0
 */
class Database extends mysqli {
    
    /**
     * 
     */
    public function query($query, $retry=true, $resultmode=MYSQLI_STORE_RESULT) {
        $result = parent::query($query, $resultmode);
        return $result;
    }
}
?>